---
layout: post
title: Pembiayaan untuk Pembangunan
---

Dalam menyelenggarakan kegiatan pemerintahan dan pembangunan, pemerintah menyusun rencana kegiatan setiap tahunnya. Rencana kegiatan tersebut nantinya didanai melalui proses perencanaan penganggaran. Proses ini dimulai dari penyusunan kebijakan tahun yang dianggarkan, penyusunan APBN, hingga terbitnya dokumen pelaksanaan anggaran yang dinamakan DIPA. Pada tulisan ini, kita lebih fokus mengenai postur APBN khususnya pada bagian pos Pembiayaan.

Belanja-belanja yang dilakukan pemerintah untuk mendanai program atau kegiatan yang dilaksanakan dapat dilihat pada APBN. Dalam APBN terbagi menjadi 3 bagian. Bagian pertama tentang Pendapatan Negara, kedua Belanja Negara, ketiga Pembiayaan. Pendapatan Negara terdiri penerimaan negara dari pajak dan penerimaan negara dari bukan pajak. Belanja Negara dibagi menjadi dua, belanja pemerintah pusat dan dana transfer ke daerah. Pembiayaan negara merupakan penerimaan atau pengeluaran uang negara sementara yang dapat dikeluarkan atau diterima dikemudian hari.

# Pembiayaan Negara
Test blehbleh...
