---
layout: post
title: Pembiayaan untuk Pembangunan Negara
---

# _**Prolog**_
Tulisan ini ditujukan untuk memberikan informasi kepada pembaca secara umum penggunaan pembiayaan utang negara. Tulisan ini digunakan sebagai peserta _blog competition_ yang diselenggarakan DJPPR.

***
<br />
<br />

Dalam menyelenggarakan kegiatan pemerintahan dan pembangunan, pemerintah menyusun rencana kegiatan setiap tahunnya. Rencana kegiatan tersebut nantinya didanai melalui proses perencanaan penganggaran. Proses ini dimulai dari penyusunan kebijakan tahun yang dianggarkan, penyusunan APBN, hingga terbitnya dokumen pelaksanaan anggaran yang dinamakan DIPA. Pada tulisan ini, kita lebih fokus mengenai bagian pos Pembiayaan.

# Sekilas Postur APBN
Belanja-belanja yang dilakukan pemerintah untuk mendanai program atau kegiatan yang dilaksanakan dapat dilihat pada APBN. Dalam APBN terbagi menjadi 3 bagian. Bagian pertama tentang Pendapatan Negara, kedua Belanja Negara, ketiga Pembiayaan. **Pendapatan Negara** terdiri penerimaan negara dari pajak dan penerimaan negara dari bukan pajak. **Belanja Negara** dibagi menjadi dua, belanja pemerintah pusat dan dana transfer ke daerah. **Pembiayaan negara** merupakan penerimaan atau pengeluaran uang negara sementara yang dapat dikeluarkan atau diterima dikemudian hari.

Bagian pembiayaan terbagi menjadi dua, penerimaan pembiayaan dan pengeluaran pembiayaan. Penerimaan pembiayaan adalah penerimaan negara yang harus dikeluarkan nantinya, Pengeluaran pembiayaan adalah pengeluaran negara yang dapat kita terima kembali. Besar pembiayaan negara tergantung pada rencana pendapatan dan belanja negara. Apabila pendapatan lebih besar dari belanja atau surplus, pemerintah dapat melakukan investasi. Namun jika belanja lebih besar dari pendapatan atau defisit, pemerintah perlu melakukan pinjaman. Pembiayaan yang dimaksud pada pernyataan diatas adalah **akumulasi total pembiayaan anggaran**. 

# Pembiayaan Negara
Pembiayaan negara terdiri dari 4 jenis, yaitu Pembiayaan Utang, Pembiayaan Investasi, Pemberian Pinjaman, Kewajiban Penjaminan, dan Pembiayaan Lainnya. **Pembiayaan utang** terdiri dari Surat Berharga Negara dan Pinjaman. **Pembiayaan Investasi** dapat diberikan kepada BUMN, Lembaga/Badan Lainnya, Badan Layanan Umum, Organisasi/Lembaga Keuangan Internasional/Badan Usaha Internasional. **Pemberian Pinjaman** terdiri dari Pinjaman kepada BUMN/Pemda dan Penerimaan Cicilan Pengembalian Pinjaman kepada BUMN/Pemda. **Kewajiban Penjaminan** merupakan dana yang dicadangkan sebagai jaminan atas resiko atas pemberian jaminan kepada K/L, Pemda, BUMN, dan/atau BUMD. Pembiayaan Lainnya terdiri dari Saldo Anggaran Lebih dan Hasil Pengelolaan Aset.

Dari jenis-jenis pembiayaan tadi, Pembiayaan Utang menjadi komposisi pembiayaan yang **paling besar**, seperti pada tahun anggaran 2019, Pembiayaan Utang direncanakan sebesar 359,3 T dibanding dengan komponen pembiayaan lain seperti Pembiayaan Investasi (75,9 T), Pembiayaan Lainnya sebesar 15 T, Pemberian Pinjaman (2,4 T), Kewajiban Penjaminan tidak dianggarkan.

# Penggunaan Pembiayaan Utang
Pembiayaan Utang terdiri dari Surat Berharga Negara dan Pinjaman. Pada tahun 2019, Surat Berharga Negara direncakan sebesar 388.957,9 M, Pinjaman Neto sebesar 29.707,3 M dibayarkan/dilunasi (pengeluaran pembiayaan). Penerbitan SBN ditujukan untuk membiayai APBN (dapat digunakan secara umum tanpa pengkhususan), kecuali sukuk karena harus memiliki _underlying asset_ sesuai prinsip syariah Islam. 

Pembiayaan negara yang berupa pinjaman terbagi menjadi 2, yaitu pinjaman dalam negeri dan pinjaman luar negeri. Pinjaman dalam negeri (neto) direncanakan sebesar 482,4 M, pinjaman luar negeri (neto) direncanakan 30.189,7 dibayarkan (pengeluaran pembiayaan). Walaupun secara neto pinjaman luar negeri adalah pengeluaran pembiayaan, bukan berarti tidak ada penerimaan pinjaman dari luar negeri. Pembiayaan dari luar negeri ditujukan untuk mendukung pendanaan proyek infrastruktur dan pengadaan barang/jasa yang belum dapat diproduksi dalam negeri. 
